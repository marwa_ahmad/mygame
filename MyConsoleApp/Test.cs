﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyConsoleApp
{
    public class Test
    {
        public static void TestHumanvsRandomPlayers()
        {
            Console.WriteLine("{0}*****************Human vs Random Players:*****************{0}", Environment.NewLine);

            Player humanP = new Player();
            humanP.Name = "human";
            humanP.MoveBehaviour = new HumanMove(GameType.PAPER);
            Console.WriteLine("Human First move is {0}", humanP.MoveBehaviour.Move.ToString());

            Player randomP = new Player();
            randomP.Name = "pc";
            randomP.MoveBehaviour = new RandomMove();
            Console.WriteLine("Random First move is {0}", randomP.MoveBehaviour.Move.ToString());


            //round 1
            Match match = new Match(humanP, randomP);
            var winner1 = match.Play();

            //round 2
            humanP.MoveBehaviour = new HumanMove(GameType.ROCK);
            Console.WriteLine("{0}Human Second move is {1}", Environment.NewLine, humanP.MoveBehaviour.Move.ToString());

            randomP.MoveBehaviour = new RandomMove();
            Console.WriteLine("Random Second move is {0}", randomP.MoveBehaviour.Move.ToString());

            var winner2 = match.Play();

            //round 3
            humanP.MoveBehaviour = new HumanMove(GameType.ROCK);
            Console.WriteLine("{0}Human Third move is {1}", Environment.NewLine, humanP.MoveBehaviour.Move.ToString());

            randomP.MoveBehaviour = new RandomMove();
            Console.WriteLine("Random Third move is {0}", randomP.MoveBehaviour.Move.ToString());

            var winner3 = match.Play();

            var matchWinner = match.GetMatchWinner();
            var message = string.Format("{0}The match winner is: {1}", Environment.NewLine, matchWinner == null ? "None" : matchWinner.Name);
            Console.WriteLine(message);

            Console.ReadLine();        
        }

        public static void TestHumanPlayers()
        {
            Console.WriteLine("{0}*****************Human Players:*****************{0}", Environment.NewLine);

            Player humanP = new Player();
            humanP.Name = "player1";
            humanP.MoveBehaviour = new HumanMove(GameType.PAPER);
            Console.WriteLine("Player1 first move is {0}", humanP.MoveBehaviour.Move.ToString());

            Player humanP2 = new Player();
            humanP2.Name = "player2";
            humanP2.MoveBehaviour = new HumanMove(GameType.ROCK);
            Console.WriteLine("Player2 first move is {0}", humanP2.MoveBehaviour.Move.ToString());


            //round 1
            Match match = new Match(humanP, humanP2);
            var winner1 = match.Play();

            //round 2
            humanP.MoveBehaviour = new HumanMove(GameType.ROCK);
            Console.WriteLine("{0}Player1 second move is {1}", Environment.NewLine, humanP.MoveBehaviour.Move.ToString());
            
            humanP2.MoveBehaviour = new HumanMove(GameType.SCISSORS);
            Console.WriteLine("Player2 second move is {0}", humanP2.MoveBehaviour.Move.ToString());

            var winner2 = match.Play();

            //round 3
            humanP.MoveBehaviour = new HumanMove(GameType.SCISSORS);
            Console.WriteLine("{0}player1 third move is {1}", Environment.NewLine, humanP.MoveBehaviour.Move.ToString());

            humanP2.MoveBehaviour = new HumanMove(GameType.PAPER);
            Console.WriteLine("Player2 third move is {0}", humanP2.MoveBehaviour.Move.ToString());

            var winner3 = match.Play();

            var matchWinner = match.GetMatchWinner();
            var message = string.Format("{0}The match winner is: {1}", Environment.NewLine, matchWinner == null ? "None" : matchWinner.Name);
            Console.WriteLine(message);

            Console.ReadLine();
        }
        
        public static void TestHumanvsTacticalRandomPlayers()
        {
            Console.WriteLine("{0}*****************Human vs Tactical Players*****************{0}", Environment.NewLine);

            Player humanP = new Player();
            humanP.Name = "human";
            humanP.MoveBehaviour = new HumanMove(GameType.PAPER);
            Console.WriteLine("Human First move is {0}", humanP.MoveBehaviour.Move.ToString());

            Player tacticP = new Player();
            tacticP.Name = "tactical pc";
            tacticP.MoveBehaviour = new TacticalMove();
            Console.WriteLine("Tactical PC First move is {0}", tacticP.MoveBehaviour.Move.ToString());
            var previousTacticMove = tacticP.MoveBehaviour.Move;

            //round 1
            Match match = new Match(humanP, tacticP);
            var winner1 = match.Play();

            //round 2
            humanP.MoveBehaviour = new HumanMove(GameType.ROCK);
            Console.WriteLine("{0}Human Second move is {1}", Environment.NewLine, humanP.MoveBehaviour.Move.ToString());

            tacticP.MoveBehaviour = new TacticalMove(new NormalTactic(previousTacticMove));
            Console.WriteLine("Tactical PC  Second move is {0}", tacticP.MoveBehaviour.Move.ToString());

            var winner2 = match.Play();

            //round 3
            humanP.MoveBehaviour = new HumanMove(GameType.ROCK);
            Console.WriteLine("{0}Human Third move is {1}", Environment.NewLine, humanP.MoveBehaviour.Move.ToString());

            previousTacticMove = tacticP.MoveBehaviour.Move;
            tacticP.MoveBehaviour = new TacticalMove(new NormalTactic(previousTacticMove));
            Console.WriteLine("Tactical PC  Third move is {0}", tacticP.MoveBehaviour.Move.ToString());

            var winner3 = match.Play();

            var matchWinner = match.GetMatchWinner();
            var message = string.Format("{0}The match winner is: {1}", Environment.NewLine, matchWinner == null ? "None" : matchWinner.Name);
            Console.WriteLine(message);

            Console.ReadLine();
        }
        
    }
}
