﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyConsoleApp
{
    public class HumanMove : IMoveBehaviour
    {
        private GameType _gameType;
        public HumanMove(GameType gametype)
        {
            _gameType = gametype;
        }
        public GameType Move
        {
            get { return _gameType; }
        }
    }
}
