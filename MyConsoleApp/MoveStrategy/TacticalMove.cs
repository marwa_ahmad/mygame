﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyConsoleApp
{
    public interface ITacticBehaviour
    {
        GameType GetTacticMove();
    }
    public class NormalTactic : ITacticBehaviour
    {
        private GameType _lastMove;
        public NormalTactic(GameType lastMove)
        {
            _lastMove = lastMove; 
        }
        public GameType GetTacticMove()
        {
            switch (_lastMove)
            {
                case GameType.ROCK:
                    return GameType.PAPER;
                case GameType.PAPER:
                    return GameType.SCISSORS;
                case GameType.SCISSORS:
                    return GameType.ROCK;
                default:
                    return GameType.ROCK;
            }         
        } 
    }
    public class AdvancedFixedTactic : ITacticBehaviour
    {        
        public GameType GetTacticMove()
        {
            return GameType.PAPER;
        }
    }
    public class TacticalMove : IMoveBehaviour
    {
        private GameType _gameType;
        public TacticalMove()
        {
            _gameType = new RandomMove().Move;
        }
        public TacticalMove(ITacticBehaviour tactic)
        {
            _gameType = tactic.GetTacticMove();
        }
        public GameType Move
        {
            get
            {
                return _gameType;
            }
        }

    }
}
