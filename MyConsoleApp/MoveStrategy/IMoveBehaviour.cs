﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyConsoleApp
{
    public enum GameType { ROCK = 1, SCISSORS = 2, PAPER = 3 }

    public interface IMoveBehaviour
    {
        GameType Move { get; }
    }
}
