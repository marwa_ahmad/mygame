﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyConsoleApp
{
    public class RandomMove : IMoveBehaviour
    {
        private GameType _gameType;

        public RandomMove()
        {
            var randomGameTypeValue = new Random().Next(1, Enum.GetValues(typeof(GameType)).Length); // 1..3
            _gameType = (GameType)randomGameTypeValue;
        }
        public GameType Move
        {
            get
            {
                return _gameType;
            }
        }
    }

}
