﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyConsoleApp
{
    public class GameRound
    {
        private IPlayer _player1;
        private IPlayer _player2;
        private IPlayer _winner;

        public GameRound(IPlayer player1, IPlayer player2)
        {
            if (player1 == null || player2 == null) throw new NullReferenceException("Players were not provided.");

            _player1 = player1;
            _player2 = player2;
            _winner = null;
        }

        /// <summary>
        /// Start the round.
        /// </summary>
        /// <returns>The winner player of this current round.</returns>
        public IPlayer Play()
        {
            var winningGameType = GetWinningGameType(_player1.MoveBehaviour.Move, _player2.MoveBehaviour.Move);

            if (winningGameType == null) return null;

            if (_player1.MoveBehaviour.Move == winningGameType)
            {
                _winner = _player1;
            }
            if (_player2.MoveBehaviour.Move == winningGameType)
            {
                _winner = _player2;
            }
            return _winner;
        }

        /// <summary>
        /// Game logic goes here
        /// </summary>
        /// <param name="gameType1"></param>
        /// <param name="gameType2"></param>
        /// <returns></returns>
        private GameType? GetWinningGameType(GameType gameType1, GameType gameType2)
        {
            if (gameType1 == gameType2)
                return null;
            if (gameType1 == GameType.ROCK && gameType2 == GameType.SCISSORS)
                return GameType.ROCK;
            if (gameType1 == GameType.ROCK && gameType2 == GameType.PAPER)
                return GameType.PAPER;

            if (gameType1 == GameType.SCISSORS && gameType2 == GameType.PAPER)
                return GameType.SCISSORS;
            if (gameType1 == GameType.SCISSORS && gameType2 == GameType.ROCK)
                return GameType.ROCK;

            if (gameType1 == GameType.PAPER && gameType2 == GameType.ROCK)
                return GameType.PAPER;

            if (gameType1 == GameType.PAPER && gameType2 == GameType.SCISSORS)
                return GameType.SCISSORS;
            return null;
        }

        public IPlayer Winner
        {
            get { return _winner; }
        }
    }
}
