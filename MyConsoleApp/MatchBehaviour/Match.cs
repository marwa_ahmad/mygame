﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace MyConsoleApp
{   
    public class Match
    {
        private List<GameRound> _rounds;
        private static int currentRoundNumber;
        public int MAXNUMBEROFROUNDS { get; private set;}
        public int WinnerMinRounds { get; private set;}
        
        private IPlayer _player1;
        private IPlayer _player2;        
        public Match(Player player1, Player player2)
        {
            if (player1 == null || player2 == null) throw new NullReferenceException("Players were not provided.");

            if(string.Compare(player1.Name, player2.Name, StringComparison.OrdinalIgnoreCase) == 0)
            {
                throw new Exception("Make sure the players' names are different.");
            }
            WinnerMinRounds = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WinnerMinRounds"]);
            MAXNUMBEROFROUNDS = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Rounds"]);
            currentRoundNumber = 0;
            _player1 = player1;
            _player2 = player2;
            _rounds = new List<GameRound>();
        }

        /// <summary>
        /// Call this method to begin or continue the match. However if you reach the max number of rounds it will return the match winner
        /// </summary>
        /// <returns></returns>
        public IPlayer Play()
        {
            if (currentRoundNumber >= MAXNUMBEROFROUNDS)
            {
                throw new Exception(string.Format("You exceeded the max rounds; {0}. Please check the winner.", MAXNUMBEROFROUNDS));
            }

            var round = new GameRound(_player1, _player2);
            var roundWinner = round.Play();
            _rounds.Add(round);
            currentRoundNumber++;
            return roundWinner;
        }

        public IPlayer GetMatchWinner()
        {
            if(currentRoundNumber != MAXNUMBEROFROUNDS) 
            {
                throw new Exception(string.Format("Please continue the match first which consists of {0}", MAXNUMBEROFROUNDS));
            }
            var winner = (from r in _rounds 
                           where r.Winner != null
                         group r by r.Winner into grpbywinner
                          where grpbywinner.Count() >= WinnerMinRounds //3-->5
                         select grpbywinner.Key).FirstOrDefault();
            return winner;                        
                        
        }
    }    
}
