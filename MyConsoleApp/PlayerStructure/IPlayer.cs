﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyConsoleApp
{   
    public interface IPlayer
    {        
        string Name { get; set; }        
        IMoveBehaviour MoveBehaviour {get; set;}
    }
}
