﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyConsoleApp
{
    public class Player : IPlayer
    {
        public string Name { get; set; }
        public IMoveBehaviour MoveBehaviour { get; set; }
    }
}
