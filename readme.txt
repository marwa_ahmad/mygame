Please follow the steps:

1. Build the solution & run.

2. Console application will appear showing the game results between human vs human, human vs random computer, and human vs tactical random computer.

3. The architecture image is beside the readme.txt.

4. The task recommendations have been applied as well.